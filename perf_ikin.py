from __future__ import print_function, division
import random
import time

from fwdmodel_num import f_kin_ref
from invmodel_num import i_kin_ref


n = 20000
testset = [f_kin_ref(random.uniform(-180, 180), random.uniform(-180, 180), random.uniform(-180, 180)) for _ in range(n)]

start = time.time()
for test in testset:
    i_kin_ref(*test)
print('{} f_kin done in {:.2f}s'.format(n, time.time()-start))