"""Fast numpy implementation of the inverse model"""
from __future__ import print_function
import math

from fwdmodel_num import f_kin_rad


EPSILON = 5
H = 41.5
U =  49.0
T = -14.5
GAMMA2 = math.atan2(22.5, 60.0)
GAMMA3 = math.atan2(93, 13.5) - math.atan2(22.5, 60.0)
D_A = math.sqrt((8+8+6.5)**2 + (36+5.5+5.5+13)**2)
D_B = math.sqrt((8+5.5)**2 + (52 + 41)**2)

def i_kin_2d(alpha, beta, d_a, d_b):

    cos_phi_b = (alpha**2 + beta**2 - d_a**2 - d_b**2) / (2 * d_a * d_b)
    solutions_phi = []

    for choice_b in [-1, 1]:
        try:
            sin_phi_b = choice_b*math.sqrt(1 - cos_phi_b**2)
        except ValueError as e:
            break

        phi_b = math.atan2(sin_phi_b, cos_phi_b)

        cos_phi_a = (alpha * (d_a + d_b * cos_phi_b) + beta * d_b * sin_phi_b) / (alpha**2 + beta**2)

        for choice_a in [-1, 1]:
            sin_phi_a =  choice_a * math.sqrt(1 - cos_phi_a**2)
            phi_a = math.atan2(sin_phi_a, cos_phi_a)

            solutions_phi.append((phi_a, phi_b))

    return solutions_phi

def i_kin_numeric(x, y, z):

    numeric_solutions = []
    theta1_ = math.atan2(y, x - H)

    for theta1 in [theta1_, theta1_ + math.pi]:

        #input for the 2D problem
        x_p   = (x - H) * math.cos( - theta1) - y * math.sin( - theta1)
        alpha = x_p - U
        beta  = z   - T

        solutions_phi = i_kin_2d(alpha, beta, D_A, D_B)

        phi_solutions = [(theta1, phi_a - GAMMA2, phi_b + GAMMA3) for phi_a, phi_b in solutions_phi]
        numeric_solutions.extend(phi_solutions)

    return numeric_solutions

def i_kin_rad(x, y, z):
    num_solutions = i_kin_numeric(x, y, z)

    filtered_solutions = []
    for t1, t2, t3 in num_solutions:
        if sum((x1_i - x2_i)**2 for x1_i, x2_i in zip(f_kin_rad(t1, t2, t3), (x, y, z))) < EPSILON:
            filtered_solutions.append((t1 % (2*math.pi), t2, t3))

    return filtered_solutions

def i_kin_ref(x, y, z):
    return [(math.degrees(t1), math.degrees(t2), math.degrees(t3)) for t1, t2, t3 in i_kin_rad(x, y, z)]
