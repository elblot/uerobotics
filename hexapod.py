from __future__ import print_function, division
import math
import time

import numpy as np

import pydyn

from fwdmodel_num import f_kin_ref as f_kin
from invmodel_num import i_kin_ref as i_kin



def rotz(angle):
    return np.matrix([[math.cos(angle), -math.sin(angle), 0],
                      [math.sin(angle),  math.cos(angle), 0],
                      [              0,                0, 1]])

LEG_FRAME = [rotz(0),
             rotz(-math.pi/2),
             rotz(-math.pi/2),
             rotz(-math.pi),
             rotz( math.pi/2),
             rotz( math.pi/2)
            ]

class Hexapod(object):

    def __init__(self, layout, moving_speed=100, timeout=20):
        self._layout = layout

        self.ms = pydyn.MotorSet(timeout=timeout, motor_range=[0, 70])
        assert len(self.ms.motors) == 18, "Only found {} motors".format(len(self.ms.motors))
        self.ms.moving_speed = moving_speed

        motormap = {m.id: m for m in self.ms.motors}
        self.legs = [pydyn.MotorSet([motormap[mid] for mid in leglayout]) for leglayout in self.layout]

    @property
    def layout(self):
        return self._layout

    @property
    def orientations(self):
        return self._orientations

    @staticmethod
    def center_angle(theta):
        return theta - 360 if theta > 210 else theta

    @staticmethod
    def possible_solution(sol):
        return ( -90 < sol[0] < 90 and
                 -90 < sol[1] < 110 and
                -150 < sol[2] < 90)

    def filter_solutions(self, leg_id, solutions):
        """Choose the best solution of inverse kinematics"""
        solutions = [(self.center_angle(s[0]), self.center_angle(s[1]), self.center_angle(s[2])) for s in solutions]
        solutions = [s for s in solutions if self.possible_solution(s)]
        d_min = float('inf')
        angles_now = self.legs[leg_id].position
        best_solution = None
        for s in solutions:
            d_s = sum((s_i - a_i)**2 for (s_i, a_i) in zip(s, angles_now))
            if d_s < d_min:
                best_solution = s
                d_min = d_s
        return best_solution

    def move(self, leg_id, dp):
        dp_prim = LEG_FRAME[leg_id]*np.matrix(dp).T
        pos_now = f_kin(*self.legs[leg_id].position)
        desired = np.array(pos_now) + dp_prim.T
        solutions = i_kin(desired[0, 0], desired[0, 1], desired[0, 2])
        best_solution = self.filter_solutions(leg_id, solutions)
        if best_solution is None:
            raise ValueError('No solution found')
        self.legs[leg_id].position = best_solution

if __name__ == '__main__':
    # the order of each leg is [theta1, theta2, theta3]
    layout = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12], [13, 14, 15], [16, 17, 18]]

    h = Hexapod(layout, timeout=20)
    h.ms.position = 0
    time.sleep(1)

    offset = 30
    for i in range(6):
        factor = -1 if i in [0, 1, 5] else 1
        h.move(i, (-offset*factor, 0, 0))
        time.sleep(0.5)
        h.move(i, ( offset*factor, 0, 0))
        time.sleep(0.5)
